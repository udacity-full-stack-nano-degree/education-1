# "Database code" for the DB Forum.

import datetime
import sqlalchemy.dialects.postgresql.psycopg2
import psycopg2 as psycopg
import sqlite3
import bleach 

DB_NAME = "forum"

# POSTS = [("This is the first post.", datetime.datetime.now())]
# POSTS = psycopg.connect("dbname=forum")


def get_posts():
  conn = psycopg.connect(database=DB_NAME)
  cur = conn.cursor()
  cur.execute("SELECT * FROM posts")
  records = cur.fetchall()
  conn.close()
  return reversed(records)

def add_post(content):
  """Add a post to the 'database' with the current timestamp."""
  # POSTS.append((content, datetime.datetime.now()))
  conn = psycopg.connect(database=DB_NAME)
  cur = conn.cursor()
  cur.execute("INSERT INTO posts VALUES (%s)", (bleach.clean(content),))
  # cur.execute("INSERT INTO posts VALUES", (content,))
  conn.commit()
  conn.close()


